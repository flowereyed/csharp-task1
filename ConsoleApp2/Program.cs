﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int size, i, j;
            int[,] A = new int[10, 10];

            Console.WriteLine("Enter matrix size: ");
            size = Convert.ToInt16(Console.ReadLine());
            
            Console.WriteLine("Enter elements of a matrix: ");
            for (i = 0; i < size; i++)
            {
                for (j = 0; j < size; j++)
                {
                    A[i, j] = Convert.ToInt16(Console.ReadLine());
                }
            }

            Console.Clear();

            Console.WriteLine("Given matrix: ");
            for (i = 0; i < size; i++)
            {
                for (j = 0; j < size; j++)
                {
                    Console.Write(A[i, j] + "\t");

                }
                Console.WriteLine();
            }

            Console.WriteLine("Transposed matrix: ");
            for (i = 0; i < size; i++)
            {
                for (j = 0; j < size; j++)
                {
                    Console.Write(A[j, i] + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}